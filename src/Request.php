<?php

    namespace rocketCRM;

    use exceptions\rocketCRM\Exception;

    /**
     * Class Request
     *
     * @package rocketCRM
     * @author Yaroslav <ya@devio.pro>
     */
    class Request
    {
        public function __call($name, $arguments)
        {
            $name = 'request' . ucfirst(strtolower($name));

            if (method_exists($this, $name)) {
                return $this->$name($arguments);
            } else {
                throw new Exception("Unknown request type");
            }
        }

        public function jsonRequest($url, $data, $params = [])
        {

        }

        public function getRequest($url, $data, $params = [])
        {

        }

        public function postRequest($url, $data, $params = [])
        {

        }

        private function request($type, $url, $data, $params)
        {

        }
    }