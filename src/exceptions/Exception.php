<?php

    namespace exceptions\rocketCRM;

    /**
     * Class Exception
     *
     * Base exceptions
     *
     * @package rocketCRM
     * @author Yaroslav <ya@devio.pro>
     */
    class Exception extends \Exception
    {
    }